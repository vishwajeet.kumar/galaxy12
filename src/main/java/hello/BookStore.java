package hello;

public class BookStore {

    private String name;

    public BookStore(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
