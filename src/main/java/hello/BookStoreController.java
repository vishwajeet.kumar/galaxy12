package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookStoreController {

    @RequestMapping("/application")
    public BookStore getStoreName() {
        return new BookStore("Book Store");
    }
}
