package hello;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


public class BookControllerTest {



    @Test
    public void shouldReturnBookStoreStringWhenInvokeApplicationEndpoint() {
        BookStoreController controller = new BookStoreController();
        BookStore store = controller.getStoreName();
        Assert.assertEquals("Book Store", store.getName());
    }
}
